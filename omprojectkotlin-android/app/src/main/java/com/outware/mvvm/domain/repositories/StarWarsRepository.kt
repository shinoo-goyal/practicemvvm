package au.com.starwars.domain.repositories

import com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper
import io.reactivex.Single

/**
 * Created by shinoo.goyal on 18/10/17.
 */
interface StarWarsRepository {
    fun getStarWarsCharacterList(): Single<StarWarsResponseWrapper>
}