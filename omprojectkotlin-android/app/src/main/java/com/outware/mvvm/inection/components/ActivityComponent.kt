package com.outware.mvvm.inection.components

import android.arch.lifecycle.ViewModelProvider
import com.outware.mvvm.inection.annotations.PerScreen
import com.outware.mvvm.inection.modules.NetworkModule
import com.outware.mvvm.inection.modules.RepositoryModule
import com.outware.mvvm.inection.modules.ViewModelFactory
import com.outware.mvvm.inection.modules.ViewModelModule
import com.outware.mvvm.presentation.view.StarWarsListActivity
import dagger.Component

@PerScreen
@Component(dependencies = [ApplicationComponent::class])
interface ActivityComponent {
    fun inject(activity: StarWarsListActivity)
}