package com.outware.mvvm.presentation.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper
import com.outware.mvvm.domain.usecases.GetStarWarsCharactersUseCase
import com.outware.mvvm.inection.components.DaggerViewModelComponent
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class StarWarsListViewModel @Inject constructor() : ViewModel() {

    @Inject
    lateinit var getStarWarsCharactersUseCase: GetStarWarsCharactersUseCase
    val compositeDisposable = CompositeDisposable()
    val starwarsData = MutableLiveData<StarWarsResponseWrapper>()

    init {
        inject()
    }

    fun inject() {
        DaggerViewModelComponent.builder().build().inject(this)
    }

    fun getCharacterList() {
        compositeDisposable.add(getStarWarsCharactersUseCase.execute()
                .subscribe({
                    starwarsData.postValue(it)
                }, {}))
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}