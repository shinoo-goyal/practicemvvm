package com.outware.mvvm.data.mappers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/outware/mvvm/data/mappers/StarWarsMapper;", "", "()V", "Companion", "app_debug"})
public final class StarWarsMapper {
    public static final com.outware.mvvm.data.mappers.StarWarsMapper.Companion Companion = null;
    
    public StarWarsMapper() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u000e\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006\u00a8\u0006\b"}, d2 = {"Lcom/outware/mvvm/data/mappers/StarWarsMapper$Companion;", "", "()V", "mapToStarWarsMapper", "Lcom/outware/mvvm/domain/models/entities/StarWarsResponseWrapper;", "list", "", "Lcom/outware/mvvm/data/api/models/StarWarsResult;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper mapToStarWarsMapper(@org.jetbrains.annotations.Nullable()
        java.util.List<com.outware.mvvm.data.api.models.StarWarsResult> list) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}