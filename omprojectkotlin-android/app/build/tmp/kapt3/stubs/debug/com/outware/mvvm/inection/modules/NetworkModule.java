package com.outware.mvvm.inection.modules;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004H\u0002J\u0010\u0010\u0006\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004H\u0007J\u0018\u0010\u0007\u001a\n \u0005*\u0004\u0018\u00010\b0\b2\u0006\u0010\t\u001a\u00020\u0004H\u0007\u00a8\u0006\n"}, d2 = {"Lcom/outware/mvvm/inection/modules/NetworkModule;", "", "()V", "buildRetrofit", "Lretrofit2/Retrofit;", "kotlin.jvm.PlatformType", "provideRetrofit", "provideStarWarsService", "Lcom/outware/mvvm/data/api/services/StarWarsService;", "retrofit", "app_debug"})
@dagger.Module()
public final class NetworkModule {
    
    @dagger.Provides()
    @javax.inject.Singleton()
    public final retrofit2.Retrofit provideRetrofit() {
        return null;
    }
    
    private final retrofit2.Retrofit buildRetrofit() {
        return null;
    }
    
    @dagger.Provides()
    @javax.inject.Singleton()
    public final com.outware.mvvm.data.api.services.StarWarsService provideStarWarsService(@org.jetbrains.annotations.NotNull()
    retrofit2.Retrofit retrofit) {
        return null;
    }
    
    public NetworkModule() {
        super();
    }
}