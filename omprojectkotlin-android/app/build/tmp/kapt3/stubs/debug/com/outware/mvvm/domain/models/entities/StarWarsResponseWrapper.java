package com.outware.mvvm.domain.models.entities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lcom/outware/mvvm/domain/models/entities/StarWarsResponseWrapper;", "", "characters", "", "Lcom/outware/mvvm/domain/models/entities/StarWarsCharacter;", "(Ljava/util/List;)V", "getCharacters", "()Ljava/util/List;", "app_debug"})
public final class StarWarsResponseWrapper {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.outware.mvvm.domain.models.entities.StarWarsCharacter> characters = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.outware.mvvm.domain.models.entities.StarWarsCharacter> getCharacters() {
        return null;
    }
    
    public StarWarsResponseWrapper(@org.jetbrains.annotations.NotNull()
    java.util.List<com.outware.mvvm.domain.models.entities.StarWarsCharacter> characters) {
        super();
    }
}