package com.outware.mvvm.inection.components

import com.outware.mvvm.DataBindingApplication
import com.outware.mvvm.inection.modules.NetworkModule
import com.outware.mvvm.inection.modules.RepositoryModule
import com.outware.mvvm.inection.modules.ViewModelModule
import com.outware.mvvm.presentation.viewmodel.StarWarsListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RepositoryModule::class, NetworkModule::class])
interface ApplicationComponent : RepositoryComponent {
    fun inject(application: DataBindingApplication)
}