package com.outware.mvvm.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0017\u0018\u00002\u00020\u0001B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\b\u0010\u0005\u001a\u00020\u0004H\u0016\u00a8\u0006\u0006"}, d2 = {"Lcom/outware/mvvm/util/Schedulers;", "", "()V", "io", "Lio/reactivex/Scheduler;", "ui", "app_debug"})
@javax.inject.Singleton()
public class Schedulers {
    
    @org.jetbrains.annotations.NotNull()
    public io.reactivex.Scheduler ui() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public io.reactivex.Scheduler io() {
        return null;
    }
    
    @javax.inject.Inject()
    public Schedulers() {
        super();
    }
}