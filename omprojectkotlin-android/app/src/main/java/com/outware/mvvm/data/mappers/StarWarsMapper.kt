package com.outware.mvvm.data.mappers

import com.outware.mvvm.data.api.models.StarWarsResult
import com.outware.mvvm.domain.models.entities.StarWarsCharacter
import com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper

class StarWarsMapper {
    companion object {
        fun mapToStarWarsMapper(list: List<StarWarsResult>?): StarWarsResponseWrapper {
            return if (list != null)
                StarWarsResponseWrapper(list.map { StarWarsCharacter(it.name, it.birthYear, it.species.get(0)) })
            else
                StarWarsResponseWrapper(emptyList())
        }
    }
}