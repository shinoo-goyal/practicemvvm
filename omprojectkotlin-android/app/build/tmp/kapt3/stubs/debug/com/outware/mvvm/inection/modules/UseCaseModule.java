package com.outware.mvvm.inection.modules;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0004"}, d2 = {"Lcom/outware/mvvm/inection/modules/UseCaseModule;", "", "bindGetStarWarsListUseCase", "Lcom/outware/mvvm/domain/usecases/GetStarWarsCharactersUseCase;", "app_debug"})
@dagger.Module()
public abstract interface UseCaseModule {
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.outware.mvvm.domain.usecases.GetStarWarsCharactersUseCase bindGetStarWarsListUseCase();
}