package com.outware.mvvm.presentation.view;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0012B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0007\u001a\u00020\bH\u0016J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\bH\u0016J\u0018\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\bH\u0016J\u0014\u0010\u0010\u001a\u00020\n2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/outware/mvvm/presentation/view/StarWarsAdapter;", "Landroid/support/v7/widget/RecyclerView$Adapter;", "Lcom/outware/mvvm/presentation/view/StarWarsAdapter$StarWarsItemViewHolder;", "()V", "starWarsList", "", "Lcom/outware/mvvm/domain/models/entities/StarWarsCharacter;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "setStarWarsList", "list", "StarWarsItemViewHolder", "app_debug"})
public final class StarWarsAdapter extends android.support.v7.widget.RecyclerView.Adapter<com.outware.mvvm.presentation.view.StarWarsAdapter.StarWarsItemViewHolder> {
    private java.util.List<com.outware.mvvm.domain.models.entities.StarWarsCharacter> starWarsList;
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.outware.mvvm.presentation.view.StarWarsAdapter.StarWarsItemViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int position) {
        return null;
    }
    
    public final void setStarWarsList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.outware.mvvm.domain.models.entities.StarWarsCharacter> list) {
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.outware.mvvm.presentation.view.StarWarsAdapter.StarWarsItemViewHolder holder, int position) {
    }
    
    public StarWarsAdapter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"Lcom/outware/mvvm/presentation/view/StarWarsAdapter$StarWarsItemViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bindData", "", "starWarsCharacter", "Lcom/outware/mvvm/domain/models/entities/StarWarsCharacter;", "app_debug"})
    public static final class StarWarsItemViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        
        public final void bindData(@org.jetbrains.annotations.NotNull()
        com.outware.mvvm.domain.models.entities.StarWarsCharacter starWarsCharacter) {
        }
        
        public StarWarsItemViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}