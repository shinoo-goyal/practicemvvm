package com.outware.mvvm;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\t\u001a\u00020\nJ\b\u0010\u000b\u001a\u00020\nH\u0016R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\f"}, d2 = {"Lcom/outware/mvvm/DataBindingApplication;", "Landroid/app/Application;", "()V", "applicationComponent", "Lcom/outware/mvvm/inection/components/ApplicationComponent;", "getApplicationComponent", "()Lcom/outware/mvvm/inection/components/ApplicationComponent;", "setApplicationComponent", "(Lcom/outware/mvvm/inection/components/ApplicationComponent;)V", "inject", "", "onCreate", "app_debug"})
public final class DataBindingApplication extends android.app.Application {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.outware.mvvm.inection.components.ApplicationComponent applicationComponent;
    
    @org.jetbrains.annotations.NotNull()
    public final com.outware.mvvm.inection.components.ApplicationComponent getApplicationComponent() {
        return null;
    }
    
    public final void setApplicationComponent(@org.jetbrains.annotations.NotNull()
    com.outware.mvvm.inection.components.ApplicationComponent p0) {
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    public final void inject() {
    }
    
    public DataBindingApplication() {
        super();
    }
}