package com.outware.mvvm.data.api;

import java.lang.System;

/**
 * * Created by azfar.siddiqui on 21/9/17.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0002J\u001f\u0010\u0003\u001a\u0002H\u0004\"\u0004\b\u0000\u0010\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lcom/outware/mvvm/data/api/CallCoordinator;", "", "()V", "execute", "T", "call", "Lretrofit2/Call;", "(Lretrofit2/Call;)Ljava/lang/Object;", "app_debug"})
@javax.inject.Singleton()
public final class CallCoordinator {
    
    /**
     * * Synchronously executes the provided call and, if successful, returns the parsed response
     *     * body. If the call is not successful an attempt is made to parse the response error body. Any
     *     * exceptions are mapped to domain level exceptions.
     */
    public final <T extends java.lang.Object>T execute(@org.jetbrains.annotations.NotNull()
    retrofit2.Call<T> call) {
        return null;
    }
    
    @javax.inject.Inject()
    public CallCoordinator() {
        super();
    }
}