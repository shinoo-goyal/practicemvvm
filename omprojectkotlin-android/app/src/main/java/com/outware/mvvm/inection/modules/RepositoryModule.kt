package com.outware.mvvm.inection.modules

import au.com.starwars.domain.repositories.StarWarsRepository
import com.outware.mvvm.data.managers.StarWarsManager
import com.outware.mvvm.inection.annotations.PerScreen
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
interface RepositoryModule {
    @Binds
    @Singleton
    fun bindStarWarsRepository(starWarsManager: StarWarsManager): StarWarsRepository
}