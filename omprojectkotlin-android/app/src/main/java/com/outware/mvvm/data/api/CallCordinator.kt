package com.outware.mvvm.data.api

import retrofit2.Call
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by azfar.siddiqui on 21/9/17.
 */
@Singleton
class CallCoordinator @Inject constructor() {
    /**
     * Synchronously executes the provided call and, if successful, returns the parsed response
     * body. If the call is not successful an attempt is made to parse the response error body. Any
     * exceptions are mapped to domain level exceptions.
     */
    fun <T> execute(call: Call<T>): T {
        try {
            val response = call.execute()

            if (response.isSuccessful) {
                return response.body()
            } else {
                throw Exception()
            }
        } catch (exception: Exception) {
            throw exception
        }
    }

}

