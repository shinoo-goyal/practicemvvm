package com.outware.mvvm.presentation.view;

import java.lang.System;

/**
 * * @author joelschmidt
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\bJ\r\u0010\t\u001a\u00020\nH\u0016\u00a2\u0006\u0002\u0010\u000bJ\u0012\u0010\f\u001a\u00020\b2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J\b\u0010\u000f\u001a\u00020\bH\u0002J\b\u0010\u0010\u001a\u00020\bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"Lcom/outware/mvvm/presentation/view/StarWarsListActivity;", "Lcom/outware/mvvm/presentation/view/BaseActivity;", "()V", "listAdapter", "Lcom/outware/mvvm/presentation/view/StarWarsAdapter;", "viewModel", "Lcom/outware/mvvm/presentation/viewmodel/StarWarsListViewModel;", "getStarWarsCharacterList", "", "injectActivity", "error/NonExistentClass", "()Lerror/NonExistentClass;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setupAdapters", "setupObservers", "app_debug"})
public final class StarWarsListActivity extends com.outware.mvvm.presentation.view.BaseActivity {
    private com.outware.mvvm.presentation.view.StarWarsAdapter listAdapter;
    private com.outware.mvvm.presentation.viewmodel.StarWarsListViewModel viewModel;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public error.NonExistentClass injectActivity() {
        return null;
    }
    
    public final void getStarWarsCharacterList() {
    }
    
    private final void setupAdapters() {
    }
    
    private final void setupObservers() {
    }
    
    @javax.inject.Inject()
    public StarWarsListActivity() {
        super();
    }
}