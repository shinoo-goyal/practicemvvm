package com.outware.mvvm.presentation.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0012\u001a\u00020\u0013J\u0006\u0010\u0014\u001a\u00020\u0013J\b\u0010\u0015\u001a\u00020\u0013H\u0014R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001e\u0010\u0007\u001a\u00020\b8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006\u0016"}, d2 = {"Lcom/outware/mvvm/presentation/viewmodel/StarWarsListViewModel;", "Landroid/arch/lifecycle/ViewModel;", "()V", "compositeDisposable", "Lio/reactivex/disposables/CompositeDisposable;", "getCompositeDisposable", "()Lio/reactivex/disposables/CompositeDisposable;", "getStarWarsCharactersUseCase", "Lcom/outware/mvvm/domain/usecases/GetStarWarsCharactersUseCase;", "getGetStarWarsCharactersUseCase", "()Lcom/outware/mvvm/domain/usecases/GetStarWarsCharactersUseCase;", "setGetStarWarsCharactersUseCase", "(Lcom/outware/mvvm/domain/usecases/GetStarWarsCharactersUseCase;)V", "starwarsData", "Landroid/arch/lifecycle/MutableLiveData;", "Lcom/outware/mvvm/domain/models/entities/StarWarsResponseWrapper;", "getStarwarsData", "()Landroid/arch/lifecycle/MutableLiveData;", "getCharacterList", "", "inject", "onCleared", "app_debug"})
public final class StarWarsListViewModel extends android.arch.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.outware.mvvm.domain.usecases.GetStarWarsCharactersUseCase getStarWarsCharactersUseCase;
    @org.jetbrains.annotations.NotNull()
    private final io.reactivex.disposables.CompositeDisposable compositeDisposable = null;
    @org.jetbrains.annotations.NotNull()
    private final android.arch.lifecycle.MutableLiveData<com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper> starwarsData = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.outware.mvvm.domain.usecases.GetStarWarsCharactersUseCase getGetStarWarsCharactersUseCase() {
        return null;
    }
    
    public final void setGetStarWarsCharactersUseCase(@org.jetbrains.annotations.NotNull()
    com.outware.mvvm.domain.usecases.GetStarWarsCharactersUseCase p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.disposables.CompositeDisposable getCompositeDisposable() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.arch.lifecycle.MutableLiveData<com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper> getStarwarsData() {
        return null;
    }
    
    public final void inject() {
    }
    
    public final void getCharacterList() {
    }
    
    @java.lang.Override()
    protected void onCleared() {
    }
    
    @javax.inject.Inject()
    public StarWarsListViewModel() {
        super();
    }
}