package com.outware.mvvm.inection.modules;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'\u00a8\u0006\u0006"}, d2 = {"Lcom/outware/mvvm/inection/modules/RepositoryModule;", "", "bindStarWarsRepository", "Lau/com/starwars/domain/repositories/StarWarsRepository;", "starWarsManager", "Lcom/outware/mvvm/data/managers/StarWarsManager;", "app_debug"})
@dagger.Module()
public abstract interface RepositoryModule {
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Binds()
    public abstract au.com.starwars.domain.repositories.StarWarsRepository bindStarWarsRepository(@org.jetbrains.annotations.NotNull()
    com.outware.mvvm.data.managers.StarWarsManager starWarsManager);
}