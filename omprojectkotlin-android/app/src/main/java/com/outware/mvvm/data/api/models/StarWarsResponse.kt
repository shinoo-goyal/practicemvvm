package com.outware.mvvm.data.api.models

import com.google.gson.annotations.SerializedName

data class StarWarsResponse(@SerializedName("results") val results: List<StarWarsResult>)

data class StarWarsResult(@SerializedName("name") val name: String,
                          @SerializedName("birth_year") val birthYear: String,
                          @SerializedName("species") val species: List<String>)