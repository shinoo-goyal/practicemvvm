package com.outware.mvvm.domain.usecases;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/outware/mvvm/domain/usecases/GetStarWarsCharactersUseCase;", "", "starWarsRepository", "Lau/com/starwars/domain/repositories/StarWarsRepository;", "schedulers", "Lcom/outware/mvvm/util/Schedulers;", "(Lau/com/starwars/domain/repositories/StarWarsRepository;Lcom/outware/mvvm/util/Schedulers;)V", "execute", "Lio/reactivex/Single;", "Lcom/outware/mvvm/domain/models/entities/StarWarsResponseWrapper;", "app_debug"})
public final class GetStarWarsCharactersUseCase {
    private final au.com.starwars.domain.repositories.StarWarsRepository starWarsRepository = null;
    private final com.outware.mvvm.util.Schedulers schedulers = null;
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Single<com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper> execute() {
        return null;
    }
    
    @javax.inject.Inject()
    public GetStarWarsCharactersUseCase(@org.jetbrains.annotations.NotNull()
    au.com.starwars.domain.repositories.StarWarsRepository starWarsRepository, @org.jetbrains.annotations.NotNull()
    com.outware.mvvm.util.Schedulers schedulers) {
        super();
    }
}