package com.outware.mvvm.presentation.viewmodel

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

class BaseViewModel : ViewModel() {

    val compositeDisposable = CompositeDisposable()
}