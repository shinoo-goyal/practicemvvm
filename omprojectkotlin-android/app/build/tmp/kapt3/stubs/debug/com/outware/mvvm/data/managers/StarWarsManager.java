package com.outware.mvvm.data.managers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/outware/mvvm/data/managers/StarWarsManager;", "Lau/com/starwars/domain/repositories/StarWarsRepository;", "starWarsService", "Lcom/outware/mvvm/data/api/services/StarWarsService;", "callCoordinator", "Lcom/outware/mvvm/data/api/CallCoordinator;", "(Lcom/outware/mvvm/data/api/services/StarWarsService;Lcom/outware/mvvm/data/api/CallCoordinator;)V", "getStarWarsCharacterList", "Lio/reactivex/Single;", "Lcom/outware/mvvm/domain/models/entities/StarWarsResponseWrapper;", "app_debug"})
public final class StarWarsManager implements au.com.starwars.domain.repositories.StarWarsRepository {
    private final com.outware.mvvm.data.api.services.StarWarsService starWarsService = null;
    private final com.outware.mvvm.data.api.CallCoordinator callCoordinator = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Single<com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper> getStarWarsCharacterList() {
        return null;
    }
    
    @javax.inject.Inject()
    public StarWarsManager(@org.jetbrains.annotations.NotNull()
    com.outware.mvvm.data.api.services.StarWarsService starWarsService, @org.jetbrains.annotations.NotNull()
    com.outware.mvvm.data.api.CallCoordinator callCoordinator) {
        super();
    }
}