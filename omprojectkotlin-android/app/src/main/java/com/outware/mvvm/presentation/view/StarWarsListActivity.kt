package com.outware.mvvm.presentation.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.outware.mvvm.R
import com.outware.mvvm.presentation.viewmodel.StarWarsListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

/**
 * @author joelschmidt
 */
class StarWarsListActivity @Inject constructor() : BaseActivity() {

    private var listAdapter = StarWarsAdapter()
    private lateinit var viewModel: StarWarsListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectActivity()
        setContentView(R.layout.activity_main)
        setupAdapters()
        setupObservers()
        getStarWarsCharacterList()
    }

    override fun injectActivity() = getAppInjector().inject(this)

    fun getStarWarsCharacterList() {
        viewModel.getCharacterList()
    }

    private fun setupAdapters() {
        list_characters.adapter = listAdapter
    }

    private fun setupObservers() {
        viewModel = ViewModelProviders.of(this).get(StarWarsListViewModel::class.java)
        viewModel.starwarsData.observe(this, Observer {
            it?.let {
                listAdapter.setStarWarsList(it.characters)
            }
        })
    }
}
