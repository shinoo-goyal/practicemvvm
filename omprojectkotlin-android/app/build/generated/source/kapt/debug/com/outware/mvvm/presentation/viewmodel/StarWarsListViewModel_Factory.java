// Generated by dagger.internal.codegen.ComponentProcessor (https://google.github.io/dagger).
package com.outware.mvvm.presentation.viewmodel;

import com.outware.mvvm.domain.usecases.GetStarWarsCharactersUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;

public final class StarWarsListViewModel_Factory implements Factory<StarWarsListViewModel> {
  private final Provider<GetStarWarsCharactersUseCase> getStarWarsCharactersUseCaseProvider;

  public StarWarsListViewModel_Factory(
      Provider<GetStarWarsCharactersUseCase> getStarWarsCharactersUseCaseProvider) {
    this.getStarWarsCharactersUseCaseProvider = getStarWarsCharactersUseCaseProvider;
  }

  @Override
  public StarWarsListViewModel get() {
    StarWarsListViewModel instance = new StarWarsListViewModel();
    StarWarsListViewModel_MembersInjector.injectGetStarWarsCharactersUseCase(
        instance, getStarWarsCharactersUseCaseProvider.get());
    return instance;
  }

  public static Factory<StarWarsListViewModel> create(
      Provider<GetStarWarsCharactersUseCase> getStarWarsCharactersUseCaseProvider) {
    return new StarWarsListViewModel_Factory(getStarWarsCharactersUseCaseProvider);
  }

  public static StarWarsListViewModel newStarWarsListViewModel() {
    return new StarWarsListViewModel();
  }
}
