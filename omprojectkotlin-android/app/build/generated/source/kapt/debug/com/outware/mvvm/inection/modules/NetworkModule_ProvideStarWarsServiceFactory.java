// Generated by dagger.internal.codegen.ComponentProcessor (https://google.github.io/dagger).
package com.outware.mvvm.inection.modules;

import com.outware.mvvm.data.api.services.StarWarsService;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;
import retrofit2.Retrofit;

public final class NetworkModule_ProvideStarWarsServiceFactory implements Factory<StarWarsService> {
  private final NetworkModule module;

  private final Provider<Retrofit> retrofitProvider;

  public NetworkModule_ProvideStarWarsServiceFactory(
      NetworkModule module, Provider<Retrofit> retrofitProvider) {
    this.module = module;
    this.retrofitProvider = retrofitProvider;
  }

  @Override
  public StarWarsService get() {
    return Preconditions.checkNotNull(
        module.provideStarWarsService(retrofitProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<StarWarsService> create(
      NetworkModule module, Provider<Retrofit> retrofitProvider) {
    return new NetworkModule_ProvideStarWarsServiceFactory(module, retrofitProvider);
  }
}
